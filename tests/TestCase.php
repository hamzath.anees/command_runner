<?php
namespace Hamzath\CommandRunner\Test;

use Hamzath\CommandRunner\CommandRunnerFacade;
use Hamzath\CommandRunner\CommandRunnerServiceProvider;
use Orchestra\Testbench\TestCase as OrchestraTestCase;

class TestCase extends OrchestraTestCase
{
	/**
	 * Load package service provider
	 * @param  \Illuminate\Foundation\Application $app
	 * @return Hamzath\CommandRunner\CommandRunnerServiceProvider
	 */
	protected function getPackageProviders($app)
	{
		return [CommandRunnerServiceProvider::class];
	}

	/**
	 * Load package alias
	 * @param  \Illuminate\Foundation\Application $app
	 * @return array
	 */
	protected function getPackageAliases($app)
	{
		return [
			'CommandRunner' => CommandRunnerFacade::class,
		];
	}
}