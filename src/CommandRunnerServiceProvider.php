<?php
namespace Hamzath\CommandRunner;
use Illuminate\Support\ServiceProvider;

class CommandRunnerServiceProvider extends ServiceProvider
{
	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->singleton(CommandRunner::class, function () {
			return new CommandRunner();
		});

		$this->app->alias(CommandRunner::class, 'simple-package');
	}
}