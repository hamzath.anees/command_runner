<?php
namespace Hamzath\CommandRunner;

class CommandRunner extends PHPCommandRunner
{
	public function onUnhandled($command)
	{
		echo "No handler defined for command '$command`.\n";
		return true;
	}

	//handle the "hello" command
	public function onHelloCommand($args)
	{
		echo "Hello, world!\n";
		return true;
	}

	//handle the "goodbye" command
	public function onGoodbyeCommand($args)
	{
		echo "Goodbye, world!\n";
		return true;
	}
}