<?php
namespace Hamzath\CommandRunner;

abstract class PHPCommandRunner {
	abstract function onUnhandled($command);

	public function processArguments($argc, $argv)
	{
		//were there enough args passed from the command line?
		if ($argc <= 1) {
			echo "Not enough arguments.\n";
			exit(1);
		}
		//get the task name, then calculate the name of the method to call,
		//for example, "onSaveCommand".
		for($i = 1; $i<$argc; $i++) {
			$commandName = $argv[$i];
			$methodName = "on".ucfirst(strtolower($commandName))."Command";

			//if the method exists in the class implementation, call it, otherwise
			//it's unhandled.
			if (method_exists(Static::class, $methodName)) {
				call_user_func_array(array(Static::class, $methodName), ['name'=>$commandName, 'runner'=>$this]);
			} else {
				$this->onUnhandled($commandName);  //valid command name, but no handler specified.
			}
		}
	}

	public function run($argc, $argv)
	{
		$argv = $this->processArguments($argc, $argv);
		print_r($argv);
	}
}