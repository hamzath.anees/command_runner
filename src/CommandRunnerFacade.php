<?php
namespace Hamzath\CommandRunner;
use Illuminate\Support\Facades\Facade;

class CommandRunnerFacade extends Facade
{
	protected static function getFacadeAccessor()
	{
		return 'command_runner';
	}
}